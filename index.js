require('newrelic');
const express = require('express')
const cors = require('cors')
const axios = require('axios')
const app = express()
const port = 3000

app.use(cors())

app.get('/', async (req, res) => {
    console.log(req.headers)
    try {
        const allPokemons = await axios.get('https://pokeapi.co/api/v2/pokemon')

        res.send({ success: true, data: allPokemons?.data })
    } catch (error) {
        res.send({ success: false, error })
    }
})

app.get('/?:name', async (req, res) => {
    console.log(req.headers)
    try {
        const pokemonName = req.params.name
        const pokemons = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}/`)

        res.send({ success: true, data: pokemons?.data })
    } catch (error) {
        res.send({ success: false, error })
    }
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
